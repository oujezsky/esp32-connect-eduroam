// Created by Antonín Svitáček 
#include <WiFi.h>
#include <esp_wpa2.h>
#include <esp_wifi.h>
#include <time.h>
#include <ctype.h>
#include "libb64/cdecode.h"


const char *ssid = "eduroam"; // Eduroam ssid
#define EAP_IDENTITY "xxxxxx@muni.cz" //uco@muni.cz
#define EAP_PASSWORD "xxxxxxxxxxxxxxxxxxx"     //your secondary password
#define CA_CERT_LENGTH 2000 // max length of decoded cert

uint8_t ca_pem_decoded[CA_CERT_LENGTH];
int ca_pem_decoded_length;


static const char ca_pem[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIFazCCA1OgAwIBAgIRAIIQz7DSQONZRGPgu2OCiwAwDQYJKoZIhvcNAQELBQAw
TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh
cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMTUwNjA0MTEwNDM4
WhcNMzUwNjA0MTEwNDM4WjBPMQswCQYDVQQGEwJVUzEpMCcGA1UEChMgSW50ZXJu
ZXQgU2VjdXJpdHkgUmVzZWFyY2ggR3JvdXAxFTATBgNVBAMTDElTUkcgUm9vdCBY
MTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAK3oJHP0FDfzm54rVygc
h77ct984kIxuPOZXoHj3dcKi/vVqbvYATyjb3miGbESTtrFj/RQSa78f0uoxmyF+
0TM8ukj13Xnfs7j/EvEhmkvBioZxaUpmZmyPfjxwv60pIgbz5MDmgK7iS4+3mX6U
A5/TR5d8mUgjU+g4rk8Kb4Mu0UlXjIB0ttov0DiNewNwIRt18jA8+o+u3dpjq+sW
T8KOEUt+zwvo/7V3LvSye0rgTBIlDHCNAymg4VMk7BPZ7hm/ELNKjD+Jo2FR3qyH
B5T0Y3HsLuJvW5iB4YlcNHlsdu87kGJ55tukmi8mxdAQ4Q7e2RCOFvu396j3x+UC
B5iPNgiV5+I3lg02dZ77DnKxHZu8A/lJBdiB3QW0KtZB6awBdpUKD9jf1b0SHzUv
KBds0pjBqAlkd25HN7rOrFleaJ1/ctaJxQZBKT5ZPt0m9STJEadao0xAH0ahmbWn
OlFuhjuefXKnEgV4We0+UXgVCwOPjdAvBbI+e0ocS3MFEvzG6uBQE3xDk3SzynTn
jh8BCNAw1FtxNrQHusEwMFxIt4I7mKZ9YIqioymCzLq9gwQbooMDQaHWBfEbwrbw
qHyGO0aoSCqI3Haadr8faqU9GY/rOPNk3sgrDQoo//fb4hVC1CLQJ13hef4Y53CI
rU7m2Ys6xt0nUW7/vGT1M0NPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNV
HRMBAf8EBTADAQH/MB0GA1UdDgQWBBR5tFnme7bl5AFzgAiIyBpY9umbbjANBgkq
hkiG9w0BAQsFAAOCAgEAVR9YqbyyqFDQDLHYGmkgJykIrGF1XIpu+ILlaS/V9lZL
ubhzEFnTIZd+50xx+7LSYK05qAvqFyFWhfFQDlnrzuBZ6brJFe+GnY+EgPbk6ZGQ
3BebYhtF8GaV0nxvwuo77x/Py9auJ/GpsMiu/X1+mvoiBOv/2X/qkSsisRcOj/KK
NFtY2PwByVS5uCbMiogziUwthDyC3+6WVwW6LLv3xLfHTjuCvjHIInNzktHCgKQ5
ORAzI4JMPJ+GslWYHb4phowim57iaztXOoJwTdwJx4nLCgdNbOhdjsnvzqvHu7Ur
TkXWStAmzOVyyghqpZXjFaH3pO3JLF+l+/+sKAIuvtd7u+Nxe5AW0wdeRlN8NwdC
jNPElpzVmbUq4JUagEiuTDkHzsxHpFKVK7q4+63SM1N95R1NbdWhscdCb+ZAJzVc
oyi3B43njTOQ5yOf+1CceWxG1bQVs5ZufpsMljq4Ui0/1lvh+wjChP4kqKOJ2qxq
4RgqsahDYVvTH9w7jXbyLeiNdd8XM2w9U/t7y0Ff/9yi0GE44Za4rF2LN9d11TPA
mRGunUHBcnWEvgJBQl9nJEiU0Zsnvgc/ubhPgXRR4Xq37Z0j4r7g1SgEEzwxA57d
emyPxgcYxn/eR44/KJ4EBs+lVDR3veyJm+kXQ99b21/+jh5Xos1AnX5iItreGCc=
-----END CERTIFICATE-----
)EOF";

int decode_ca_pem(const char *ca_pem, const uint8_t *decoded_ca_pem){
  int i = 0;
  char ca_pem_temp[CA_CERT_LENGTH];
  int ca_pem_temp_length = 0;
  while(ca_pem[i] != '\0'){ // filter out all characters that are not printable
    char current_char = ca_pem[i];
    if(isprint(current_char)){
      ca_pem_temp[ca_pem_temp_length] = current_char;
      ca_pem_temp_length++;
      if(ca_pem_temp_length >= CA_CERT_LENGTH){
        Serial.println("CA cert is too long, you must increment CA_CERT_LENGTH");
        return 0;
      }
    }
    i++;
  }
  ca_pem_temp[ca_pem_temp_length] = '\0'; // terminate string with zero
  const char *end_cert = "-----END CERTIFICATE-----";
  const char *begin_cert = "-----BEGIN CERTIFICATE-----";
  const int begin_cert_length = strlen(begin_cert);
  const int end_cert_length = strlen(end_cert);
  return base64_decode_chars((const char*)(&ca_pem_temp[begin_cert_length]), 
                                            ca_pem_temp_length - begin_cert_length - end_cert_length, 
                                            (char*)decoded_ca_pem);
}

uint8_t counter = 0;

void setup()
{
  Serial.begin(115200);
  ca_pem_decoded_length = decode_ca_pem(ca_pem, ca_pem_decoded);
  delay(10);
  Serial.println();
  WiFi.disconnect(true); //make sure everything is disconnected
  WiFi.mode(WIFI_STA);   //init wifi mode

  Serial.print("MAC >> ");
  Serial.println(WiFi.macAddress());
  Serial.printf("Connecting to WiFi: %s ", ssid);
  esp_wifi_sta_wpa2_ent_set_ca_cert((const unsigned char *)ca_pem_decoded, ca_pem_decoded_length);
  esp_wifi_sta_wpa2_ent_set_identity((uint8_t *)EAP_IDENTITY, strlen(EAP_IDENTITY));
  esp_wifi_sta_wpa2_ent_set_username((uint8_t *)EAP_IDENTITY, strlen(EAP_IDENTITY));
  esp_wifi_sta_wpa2_ent_set_password((uint8_t *)EAP_PASSWORD, strlen(EAP_PASSWORD));
  esp_wifi_sta_wpa2_ent_enable();
  WiFi.begin(ssid);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(F("."));
    counter++;
    if (counter >= 60)
    { //after 30 seconds timeout - reset board
      ESP.restart();
    }
  }
  Serial.println(F("ESP is now connected!"));
  Serial.print(F("IP address set: "));
  Serial.println(WiFi.localIP()); //print current IP address
  
}

void loop()
{

}
