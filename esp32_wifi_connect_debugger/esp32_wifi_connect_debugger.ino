// Created by Antonín Svitáček 
#include <Arduino.h>
#include <WiFi.h>
#include "libb64/cdecode.h"
#include "esp_wpa2.h"
#include "esp_wifi.h"
#include "PCAP.h"
#include <TimeLib.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>


#define SERIAL_CHECK_INTERVAL 500
#define SERIAL_BUFF_LENGTH 64
#define IDENTITY_LENGTH 64
#define PASSWORD_LENGTH 64
#define ANONYMOUS_IDENTITY_LENGTH 64
#define CA_CERT_LENGTH 2000
#define BAUD_RATE 921600
#define CHANNEL 1
#define MAX_CHANNEL 11 //(only necessary if channelHopping is true)
#define HOP_INTERVAL 214 //in ms (only necessary if channelHopping is true)

const char *CMD_CTRL_C = "ctrl + c";
const char *CMD_SET_IDENTITY = "set_identity";
const char *CMD_SET_ANONYMOUS_IDENTITY = "set_anonymous_identity";
const char *CMD_SET_PASSWORD = "set_password";
const char *CMD_SET_CA_CERT = "set_ca_cert";
const char *CMD_DECODE_CA_CERT = "decode_ca_cert";
const char *CMD_CONNECT_WIFI = "connect_wifi";
const char *CMD_GET_CONFIG = "get_config";
const char *CMD_GET_CA_CERT = "get_ca_cert";
const char *CMD_GET_CA_CERT_DECODED = "get_ca_cert_decoded";
const char *CMD_WIFI_INIT = "wifi_init";
const char *CMD_CONFIG_ANONYMOUS_IDENTITY = "config_anonymous_identity";
const char *CMD_CONFIG_IDENTITY = "config_identity";
const char *CMD_CONFIG_PASSWORD = "config_password";
const char *CMD_CONFIG_CA_CERT = "config_ca_cert";
const char *CMD_CONFIG_ENABLE = "config_enable";
const char *CMD_GET_CONNECTION_STATUS = "connection_state";
const char *CMD_RESTART = "esp_restart";
const char *CMD_START_CAPTURE = "start_capture";
const char *CMD_END_CAPTURE = "end_capture";
const char *CMD_SET_CHANNEL_HOPPING = "set_channel_hopping";

unsigned long last_serial_communication;

uint8_t serial_in_use;
/***
 * returns zero if this is the last callback
*/
uint8_t (*callback_when_new_msg)(char *msg, uint8_t size);

char msg[SERIAL_BUFF_LENGTH];
const char* ssid = "eduroam"; // eduroam SSID
char identity[IDENTITY_LENGTH];
char anonymous_identity[ANONYMOUS_IDENTITY_LENGTH];
char password[PASSWORD_LENGTH];
char ca_cert[CA_CERT_LENGTH];
uint8_t ca_cert_decoded[CA_CERT_LENGTH];
int current_ca_cert_length;
int current_ca_cert_decoded_length;
wifi_config_t wifi_config;
uint8_t channel_hopping = true;
uint8_t active_capture = true;
PCAP pcap = PCAP();
int current_channel = CHANNEL;
unsigned long last_cahnnel_change = 0;

/* will be executed on every packet the ESP32 gets while beeing in promiscuous mode */
void sniffer(void *buf, wifi_promiscuous_pkt_type_t type){
  wifi_promiscuous_pkt_t* pkt = (wifi_promiscuous_pkt_t*)buf;
  wifi_pkt_rx_ctrl_t ctrl = (wifi_pkt_rx_ctrl_t)pkt->rx_ctrl;
  
  uint32_t timestamp = now(); //current timestamp 
  uint32_t microseconds = (unsigned int)(micros() - millis() * 1000); //micro seconds offset (0 - 999)
  
  pcap.newPacketSerial(timestamp, microseconds, ctrl.sig_len, pkt->payload); //send packet via Serial  
}

esp_err_t event_handler(void *ctx, system_event_t *event){ return ESP_OK; }

uint8_t read_serial(char *buff, uint8_t max_size){
    uint8_t current_length = 0;
    while (Serial.available() && current_length < max_size - 1) {
        int inChar = Serial.read();
        if(isprint(inChar)){
            buff[current_length] = (char)inChar;
            current_length++;
        }
    }
    buff[current_length] = '\0'; // zero terminating the string
    return current_length;
}

uint8_t set_identity_init(){
    Serial.println("Please enter your identity on Eduroam network");
    return true;
}

uint8_t set_identity_callback(char *msg, uint8_t size){
    strcpy(identity, msg);
    Serial.println("okay");
    return false;
}

uint8_t set_anonymous_identity_init(){
    Serial.println("Please enter your anonymous identity on Eduroam network");
    return true;
}

uint8_t set_anonymous_identity_callback(char *msg, uint8_t size){
    strcpy(anonymous_identity, msg);
    Serial.println("okay");
    return false;
}

uint8_t set_password_init(){
    Serial.println("Please enter your password on Eduroam network");
    return true;
}

uint8_t set_password_callback(char *msg, uint8_t size){
    strcpy(password, msg);
    Serial.println("okay");
    return false;
}

int ca_cert_index;
uint8_t set_ca_cert_init(){
    ca_cert_index = 0;
    Serial.println("Please enter your password on Eduroam network");
    return true;
}


uint8_t set_ca_cert_callback(char *msg, uint8_t size){
    if(ca_cert_index == 0 && strcmp(msg, "-----BEGIN CERTIFICATE-----") != 0){
        Serial.println("Your ca cert must starts with: \"-----BEGIN CERTIFICATE-----\"");
        return false;
    }
    if(ca_cert_index + size < CA_CERT_LENGTH){
        strcpy(&ca_cert[ca_cert_index], msg);
        ca_cert_index += size;
        Serial.println("okay");
        if(strcmp(msg, "-----END CERTIFICATE-----") == 0){
            current_ca_cert_length = ca_cert_index;
            ca_cert[ca_cert_index] = '\0';
            return false;
        }
        return true;
    }
    Serial.println("CA cert is too long");
    return false;
}

uint8_t check_for_entered_command(char *msg, uint8_t size){
    if(strcmp(msg, CMD_SET_ANONYMOUS_IDENTITY) == 0){
        serial_in_use = set_anonymous_identity_init();
        callback_when_new_msg = &set_anonymous_identity_callback;
        return true;
    }
    if(strcmp(msg, CMD_SET_IDENTITY) == 0){
        serial_in_use = set_identity_init();
        callback_when_new_msg = &set_identity_callback;
        return true;
    }
    if(strcmp(msg, CMD_SET_PASSWORD) == 0){
        serial_in_use = set_password_init();
        callback_when_new_msg = &set_password_callback;
        return true;
    }
    if(strcmp(msg, CMD_SET_CA_CERT) == 0){
        serial_in_use = set_ca_cert_init();
        callback_when_new_msg = &set_ca_cert_callback;
        return true;
    }
    if(strcmp(msg, CMD_DECODE_CA_CERT) == 0){
        // int base64_decode_chars(const char* code_in, const int length_in, char* plaintext_out);
        const char *end_cert = "-----END CERTIFICATE-----";
        const char *begin_cert = "-----BEGIN CERTIFICATE-----";
        const int begin_cert_length = strlen(begin_cert);
        const int end_cert_length = strlen(end_cert);
        current_ca_cert_decoded_length = base64_decode_chars((const char*)(&ca_cert[begin_cert]), 
                                            current_ca_cert_length - begin_cert_length - end_cert_length, 
                                            (char*)ca_cert_decoded);
        return true;
    }

    if(strcmp(msg, CMD_WIFI_INIT) == 0){
        WiFi.disconnect(true);  //disconnect from wifi to set new wifi connection
        WiFi.mode(WIFI_STA); //init wifi mode
        return true;
    }
    if(strcmp(msg, CMD_CONNECT_WIFI) == 0){
        WiFi.begin(ssid); //connect to wifi
        return true;
    }
    if(strcmp(msg, CMD_RESTART) == 0){
        ESP.restart();
        return true;
    }
    if(strcmp(msg, CMD_CONFIG_ENABLE) == 0){
        esp_wifi_sta_wpa2_ent_enable();
        return true;
    }
    if(strcmp(msg, CMD_CONFIG_ANONYMOUS_IDENTITY) == 0){
        esp_wifi_sta_wpa2_ent_set_identity((const unsigned char*)anonymous_identity, strlen(anonymous_identity)); //provide identity
        return true;
    }
    if(strcmp(msg, CMD_CONFIG_IDENTITY) == 0){
        esp_wifi_sta_wpa2_ent_set_username((const unsigned char*)identity, strlen(identity)); //provide username
        return true;
    }
    if(strcmp(msg, CMD_CONFIG_PASSWORD) == 0){
        esp_wifi_sta_wpa2_ent_set_password((const unsigned char*)password, strlen(password)); //provide password
        return true;
    }
    if(strcmp(msg, CMD_CONFIG_CA_CERT) == 0){
        esp_wifi_sta_wpa2_ent_set_ca_cert((const unsigned char *)ca_cert_decoded, current_ca_cert_decoded_length); //provide ca_cert
        return true;
    }
    if(strcmp(msg, CMD_GET_CONNECTION_STATUS) == 0){
        if(WiFi.status() == WL_CONNECTED){
            Serial.println("ESP is connected");
            Serial.println("IP address set: ");
            Serial.println(WiFi.localIP()); //print LAN IP
        }
        else{
            Serial.println("ESP is disconnected");
        }
        return true;
    }
    if(strcmp(msg, CMD_GET_CA_CERT) == 0){
        Serial.println("-----BEGIN CERTIFICATE-----");
        for(int i = 0; i < current_ca_cert_length; i++){
            if(i % 64 == 0){
                Serial.println("");
            }
            Serial.print(ca_cert[i]);
        }
        Serial.println("-----END CERTIFICATE-----");
        return true;
    }
        if(strcmp(msg, CMD_GET_CA_CERT_DECODED) == 0){
        for(int i = 0; i < current_ca_cert_decoded_length; i++){
            Serial.printf("%x", ca_cert_decoded[i]);
            if(i != current_ca_cert_decoded_length - 1){
                Serial.print(", ");
            }
        }
        Serial.println("");
        return true;
    }
    if(strcmp(msg, CMD_GET_CONFIG) == 0){
        //esp_err_t esp_wifi_get_config(wifi_interface_t interface, wifi_config_t *conf);
        esp_wifi_get_config(WIFI_IF_STA, &wifi_config);
        Serial.printf("SSID of target AP. %s\n", wifi_config.sta.ssid);
        Serial.printf("Password of target AP. %s\n", wifi_config.sta.password);
        Serial.printf("do all channel scan or fast scan %s\n", wifi_config.sta.scan_method == 0? "WIFI_FAST_SCAN" : "WIFI_ALL_CHANNEL_SCAN");
        Serial.printf("whether set MAC address of target AP or not. Generally, station_config.bssid_set needs to be 0, and it needs to be 1 only when users need to check the MAC address of the AP. %d\n", wifi_config.sta.bssid_set);
        if(wifi_config.sta.bssid_set)
        {
            Serial.printf("MAC address of target AP: %d:%d:%d:%d:%d:%d\n", 
                                wifi_config.sta.bssid[0], 
                                wifi_config.sta.bssid[1], 
                                wifi_config.sta.bssid[2], 
                                wifi_config.sta.bssid[3], 
                                wifi_config.sta.bssid[4], 
                                wifi_config.sta.bssid[5]);
        }
        // TODO when you have time do the other configs
        Serial.printf("channel of target AP. Set to 1~13 to scan starting from the specified channel before connecting to AP. If the channel of AP is unknown, set it to 0.: %d\n", wifi_config.sta.channel);
        Serial.printf("Listen interval for ESP32 station to receive beacon when WIFI_PS_MAX_MODEM is set. Units: AP beacon intervals. Defaults to 3 if set to 0.: %d\n", wifi_config.sta.listen_interval);
        Serial.println("sort the connect AP in the list by rssi or security mode ");
        Serial.println("When scan_threshold is set, only APs which have an auth mode that is more secure than the selected auth mode and a signal stronger than the minimum RSSI will be used. ");
        Serial.println("Configuration for Protected Management Frame. Will be advertised in RSN Capabilities in RSN IE. ");
        Serial.println("Whether Radio Measurements are enabled for the connection ");
        Serial.println("Whether BSS Transition Management is enabled for the connection ");
        Serial.println("Whether MBO is enabled for the connection ");
        Serial.println("Whether FT is enabled for the connection ");
        Serial.println("Whether OWE is enabled for the connection ");
        Serial.println("Whether to enable transition disable feature ");
        Serial.println("Reserved for future feature set ");
        Serial.println("Configuration for SAE PWE derivation method ");
        Serial.println("Configuration for SAE-PK (Public Key) Authentication method ");
        Serial.println("Number of connection retries station will do before moving to next AP. scan_method should be set as WIFI_ALL_CHANNEL_SCAN to use this config." \
                        "Note: Enabling this may cause connection time to increase incase best AP doesn't behave properly. ");
        Serial.println("Whether DCM max.constellation for transmission and reception is set. ");
        Serial.println("Indicate the max.constellation for DCM in TB PPDU the STA supported. 0: not supported. 1: BPSK, 2: QPSK, 3: 16-QAM. The default value is 3. ");
        Serial.println("Indicate the max.constellation for DCM in both Data field and HE-SIG-B field the STA supported. 0: not supported. 1: BPSK, 2: QPSK, 3: 16-QAM. The default value is 3. ");
        Serial.println("Whether to support HE-MCS 0 to 9. The default value is 0. ");
        Serial.println("Whether to disable support for operation as an SU beamformee. ");
        Serial.println("Whether to disable support the transmission of SU feedback in an HE TB sounding sequence. ");
        Serial.println("Whether to disable support the transmission of partial-bandwidth MU feedback in an HE TB sounding sequence. ");
        Serial.println("Whether to disable support the transmission of CQI feedback in an HE TB sounding sequence. ");
        Serial.println("Reserved for future feature set ");
        Serial.println("Password identifier for H2E. this needs to be null terminated string ");
        return true;
    }
    if(strcmp(msg, CMD_START_CAPTURE) == 0){
        Serial.println("<<START>>");
        pcap.startSerial();
        esp_wifi_set_promiscuous(true);
        esp_wifi_set_promiscuous_rx_cb(sniffer);
        wifi_second_chan_t secondCh = (wifi_second_chan_t)NULL;
        esp_wifi_set_channel(current_channel, secondCh);
        active_capture = true;
        return true;
    }
    if(strcmp(msg, CMD_END_CAPTURE) == 0){
        esp_wifi_set_promiscuous(false);
        active_capture = false;
        return true;
    }
    return false;
}



void setup() {
    /* start Serial */
    Serial.begin(BAUD_RATE);
    delay(2000);
    last_serial_communication = millis();
    serial_in_use = false;
    active_capture = false;
}


void loop() {

    unsigned long moment = millis();

    if(moment - last_serial_communication >= SERIAL_CHECK_INTERVAL ){
        if(Serial.available() > 0){
            uint8_t length = read_serial(msg, SERIAL_BUFF_LENGTH);

            if(strcmp(msg, CMD_CTRL_C) == 0){
                serial_in_use = false;
                if(active_capture){
                    esp_wifi_set_promiscuous(false);
                    active_capture = false;
                }
                // other things you should do to return to state similar to the one whne being idle
            }

            if(serial_in_use){
                serial_in_use = callback_when_new_msg(msg, length);
            }
            else{
                if(!check_for_entered_command(msg, length)){
                    Serial.print("Unrecognized command: ");
                    Serial.println(msg);
                }
            }

        }

        last_serial_communication = millis();
    }
      /* Channel Hopping */
    if(channel_hopping && active_capture){
        unsigned long currentTime = millis();
        if(currentTime - last_cahnnel_change >= HOP_INTERVAL){
            last_cahnnel_change = currentTime;
            current_channel++; //increase channel
            if(current_channel > MAX_CHANNEL){
                current_channel = CHANNEL;
            }
            wifi_second_chan_t secondCh = (wifi_second_chan_t)NULL;
            esp_wifi_set_channel(current_channel,secondCh);
        }
    }
  
}