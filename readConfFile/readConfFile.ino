// Created by Antonín Svitáček 
#include <WiFi.h>
#include <esp_wpa2.h>
#include <esp_wifi.h>
#include <time.h>
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include <ctype.h>
#include "libb64/cdecode.h"
#include "string.h"
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

#define SCK  18
#define MISO  19
#define MOSI  23
#define CS  5
#define SD_BUS_FREQUENCY 80000000
#define EDUROAM_CONF_FILE "/eduroam.conf"
#define READ_FILE_LINE_SIZE 2000
#define EAP_IDENTITY_LENGTH 60
#define EAP_PASSWORD_LENGTH 60
#define CA_CERT_LENGTH 2000 // max length of decoded cert and PEM cert file
#define PULL_UP_BUTTON 25
#define CONFIG_CHECK_INTERVAL 3000 // in ms
#define DISPLAY_REFRESH_INTERVAL 1000 // in ms
#define WIFI_STRENGTH_CHECK_INTERVAL 1000 // in ms


const char *ssid = "eduroam"; // Eduroam ssid


char eap_identity[EAP_IDENTITY_LENGTH];
char eap_anonymous_identity[EAP_IDENTITY_LENGTH];
char eap_password[EAP_PASSWORD_LENGTH];

uint8_t ca_pem_decoded[CA_CERT_LENGTH];
int ca_pem_decoded_length;
char ca_pem[CA_CERT_LENGTH];
int ca_pem_length;

bool connfigurationSetSeccesfully;
long currentWifiStrength;

unsigned long lastConfigCheck;
unsigned long lastWifiStrengthCheck;
unsigned long lastDisplayRefresh;
unsigned long lastButtonCheck;

// *********************************************
// functions for working with oled display
// *********************************************

void refreshDisplay(){
  display.clearDisplay();

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 10);
  // Display static text
  display.println("IP:");
  display.println(WiFi.localIP());
  display.println("Singal strength:");
  display.print(currentWifiStrength);
  display.println("dBm");
  display.display(); 
}

// *********************************************
// functions for working with microSd card and 
// working with config
// *********************************************
int readFileLine(File *file, char * line){
  int lineLength = 0;
  int character = '\0';
  while(file->available() || character == '\n'){
    character = file->read();
    if(isprint(current_char)){
      line[lineLength] = current_char;
      lineLength++;
      if(lineLength >= READ_FILE_LINE_SIZE){
        Serial.println("Line in .conf file is too long, you must increment READ_FILE_LINE_SIZE");
        return -1;
      }
    }
  }
  line[lineLength] = '\0';
  return lineLength;
}

void matchStartAndCopyRest(const char *source, const char *prefix, char *destination){
  // does not solve overflow
  int prefixLength = strlen(prefix);
  for(int i = 0; i < prefixLength; i++){
    char sourceChar = source[i];
    if(sourceChar == '\n' || sourceChar != prefix[i]){
      Serial.println("Unable to match prefix");
      Serial.println(prefix);
      Serial.println("on line");
      Serial.println(source);
    }
    
  }
  if(source[prefixLength] != '='){
    Serial.println("Unable to match prefix");
    Serial.println(prefix);
    Serial.println("on line");
    Serial.println(source);
  }
  const char *startOfValue = &source[prefixLength + 1]; // +1 is for the = sign
  strcpy(destination, source);
}

void setAnonymousIdentity(const char *line){
  const char *anonymous_identity_prefix = "anonymous_identity";
  matchStartAndCopyRest(line, anonymous_identity_prefix, eap_anonymous_identity);
}
void setIdentity(const char *line){
  const char *identity_prefix = "identity";
  matchStartAndCopyRest(line, identity_prefix, eap_identity);

}
void setPassword(const char *line){
  const char *password_prefix = "password";
  matchStartAndCopyRest(line, password_prefix, eap_password);

}

int readPemFile(File *file, char * pem_file_content){
  int pemFileLength = 0;
  int character = '\0';
  while(file->available()){
    character = file->read();
    if(isprint(current_char)){
      pem_file_content[pemFileLength] = current_char;
      pemFileLength++;
      if(pemFileLength >= CA_CERT_LENGTH){
        Serial.println(".pem file is too long, you must increment CA_CERT_LENGTH");
        return -1;
      }
    }
  }
  pem_file_content[pemFileLength] = '\0';
  return pemFileLength;
}
void setCaCert(const char *line){
  const char *ca_cert_prefix = "ca_cert";
  char *ca_path[READ_FILE_LINE_SIZE];
  matchStartAndCopyRest(line, ca_cert_prefix, ca_path);
  File ca_file = fs.open(ca_path);
  if(!ca_file){
      Serial.println("Failed to open .pem file for reading");
      return -1;
  }  
  ca_pem_length = readPemFile(&ca_file, ca_pem);
  ca_file.close();
}
/*
file in format
anonymous_identity=xxxxx@muni.cz
identity=xxxxx@muni.cz
password=******************
ca_cert=./ca_cert.pem
*/
int readConfFile(fs::FS &fs){
  char line[READ_FILE_LINE_SIZE];
  File file = fs.open(EDUROAM_CONF_FILE);
  if(!file){
      Serial.println("Failed to open file for reading");
      return -1;
  }  
  
  // first line anonymous_identity=xxxxx@muni.cz
  int lineLength = readFileLine(&file, line);
  if(lineLength == -1){
    return -1;
  }
  setAnonymousIdentity(line);
    // line identity=xxxxx@muni.cz
  lineLength = readFileLine(&file, line);
  if(lineLength == -1){
    return -1;
  }
  setIdentity(line);
  // password=******************
  lineLength = readFileLine(&file, line);
  if(lineLength == -1){
    return -1;
  }
  setPassword(line);
  // ca_cert=./ca_cert.pem
  lineLength = readFileLine(&file, line);
  if(lineLength == -1){
    return -1;
  }
  file.close(); // close the file before trying to read .pem file
  setCaCert(line;)
  return 0;
}

int decode_ca_pem(const char *ca_pem, const uint8_t *decoded_ca_pem){
  int i = 0;
  char ca_pem_temp[CA_CERT_LENGTH];
  int ca_pem_temp_length = 0;
  while(ca_pem[i] != '\0'){ // filter out all characters that are not printable
    char current_char = ca_pem[i];
    if(isprint(current_char)){
      ca_pem_temp[ca_pem_temp_length] = current_char;
      ca_pem_temp_length++;
      if(ca_pem_temp_length >= CA_CERT_LENGTH){
        Serial.println("CA cert is too long, you must increment CA_CERT_LENGTH");
        return 0;
      }
    }
    i++;
  }
  ca_pem_temp[ca_pem_temp_length] = '\0'; // terminate string with zero
  const char *end_cert = "-----END CERTIFICATE-----";
  const char *begin_cert = "-----BEGIN CERTIFICATE-----";
  const int begin_cert_length = strlen(begin_cert);
  const int end_cert_length = strlen(end_cert);
  return base64_decode_chars((const char*)(&ca_pem_temp[begin_cert_length]), 
                                            ca_pem_temp_length - begin_cert_length - end_cert_length, 
                                            (char*)decoded_ca_pem);
}

void readConfigurationFromMicroSdCard(){
    SPIClass spi = SPIClass(VSPI);
  spi.begin(SCK, MISO, MOSI, CS);
  if(!SD.begin(CS,spi,SD_BUS_FREQUENCY)){
    Serial.println("Card Mount Failed");
    return;
  }
  uint8_t cardType = SD.cardType();

  if(cardType == CARD_NONE){
      Serial.println("No SD card attached");
      return;
  }
  Serial.print("SD Card Type: ");
  if(cardType == CARD_MMC){
      Serial.println("MMC");
  } else if(cardType == CARD_SD){
      Serial.println("SDSC");
  } else if(cardType == CARD_SDHC){
      Serial.println("SDHC");
  } else {
      Serial.println("UNKNOWN");
  }
  uint64_t cardSize = SD.cardSize() / (1024 * 1024);
  Serial.printf("SD Card Size: %lluMB\n", cardSize);
  if(readConfFile(SD) == -1){
    connfigurationSetSeccesfully = false;
    Serila.println("Unable to read .conf file");
  }
  ca_pem_decoded_length = decode_ca_pem(ca_pem, ca_pem_decoded);
  connfigurationSetSeccesfully = true;
}
// *********************************************
// wifi set up connection
// *********************************************
void connectToEduroam(){
  WiFi.disconnect(true); //make sure everything is disconnected
  WiFi.mode(WIFI_STA);   //init wifi mode

  Serial.print("MAC >> ");
  Serial.println(WiFi.macAddress());
  Serial.printf("Connecting to WiFi: %s ", ssid);
  esp_wifi_sta_wpa2_ent_set_ca_cert((const unsigned char *)ca_pem_decoded, ca_pem_decoded_length);
  esp_wifi_sta_wpa2_ent_set_identity((uint8_t *)eap_anonymous_identity, strlen(eap_anonymous_identity));
  esp_wifi_sta_wpa2_ent_set_username((uint8_t *)eap_identity, strlen(eap_identity));
  esp_wifi_sta_wpa2_ent_set_password((uint8_t *)eap_password, strlen(eap_password));
  esp_wifi_sta_wpa2_ent_enable();
  WiFi.begin(ssid);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(F("."));
    counter++;
    if (counter >= 60)
    { //after 30 seconds timeout - reset board
      ESP.restart();
    }
  }
  Serial.println(F("ESP is now connected!"));
  Serial.print(F("IP address set: "));
  Serial.println(WiFi.localIP()); //print current IP address
}
// *********************************************
// point of entry in this code
// *********************************************

void setup() {
  pinMode(PULL_UP_BUTTON, INPUT_PULLUP);
  Serial.begin(115200);
  readConfigurationFromMicroSdCard();
  connectToEduroam();
  lastConfigCheck = millis();
  lastWifiStrengthCheck = millis();
  lastDisplayRefresh = millis();
  lastButtonCheck = millis();
  


}

void loop() {
  
    unsigned long moment = millis();

    if(moment - lastButtonCheck >= 100 ){ // checks button for config read
      bool isPressedButon = !digitalRead(PULL_UP_BUTTON); // pullUp must be nagated
      if(isPressedButon){
        readConfigurationFromMicroSdCard();
      }
      lastButtonCheck = millis();
    }
    if(moment - lastConfigCheck >= CONFIG_CHECK_INTERVAL ){ // checks config and then wifi state
      if(connfigurationSetSeccesfully){
        if(WiFi.status() != WL_CONNECTED){
          connectToEduroam();
        }
      }
      lastConfigCheck = millis();
    }
    if(moment - lastWifiStrengthCheck >= WIFI_STRENGTH_CHECK_INTERVAL ){ // checks wifi strength
      if(WiFi.status() == WL_CONNECTED){
        currentWifiStrength = WiFi.RSSI();
      }
      else{ // checks for avaiable eduroma networks in are and their signal strength
        String eduroamString = String(ssid);
        int n = WiFi.scanNetworks();
        currentWifiStrength = -999999;
        if (n != 0) {
          for (int i = 0; i < n; ++i) {
            if(WiFi.SSID(i).equals(eduroamString)){
              long eduroamStrength = WiFi.RSSI(i);
              if(currentWifiStrength < eduroamStrength){
                eduroamStrength = eduroamStrength;
              }
            }
          }
          if(currentWifiStrength < -100){
            currentWifiStrength = 0;
          }

        }
          
      }
      lastWifiStrengthCheck = millis();
    }
    if(moment - lastDisplayRefresh >= DISPLAY_REFRESH_INTERVAL ){ // refreshes display
      refreshDisplay();
      lastDisplayRefresh = millis();
    }

}
